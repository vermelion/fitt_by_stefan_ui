import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { useNavigationRoutesConstant } from '../Constants';
import { NotFoundPage } from '../Pages';

const NotFoundRoute: React.FC = () => <Route component={NotFoundPage} />;

const RoutesNavigator: React.FC = () => {
  const NavigationRoutes = useNavigationRoutesConstant();
  return (
    <Switch>
      {NavigationRoutes.public.map(route => (
        <Route
          key={route.path}
          path={route.path}
          component={route.component}
          exact
        />
      ))}
      <NotFoundRoute />
    </Switch>
  );
};
export default React.memo(RoutesNavigator);
