import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Providers from './Context/Context.Providers';
import { RoutesNavigator } from './Navigation';

const App: React.FC = () => (
  <BrowserRouter>
    <Providers>
      <RoutesNavigator />
    </Providers>
  </BrowserRouter>
);

export default App;
