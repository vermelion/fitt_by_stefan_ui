import styled, { css } from 'styled-components';
import StyleTypes, {
  flexDirectionTypes,
  flexWrapTypes,
  alignItemsTypes,
  justifyContentTypes,
  overflowTypes
} from './Style.Types';

interface ContainerProps extends StyleTypes {
  alignItems?: alignItemsTypes;
  alignSelf?: alignItemsTypes;
  borderBottomLeftRadius?: string | number;
  borderBottomRightRadius?: string | number;
  borderRadius?: string | number;
  borderTopLeftRadius?: string | number;
  borderTopRightRadius?: string | number;
  boxShadow?: string;
  disableScrollbar?: boolean;
  flexDirection?: flexDirectionTypes;
  flexWrap?: flexWrapTypes;
  gutters?: boolean;
  justifyContent?: justifyContentTypes;
  justifySelf?: justifyContentTypes;
  overflow?: overflowTypes;
  overflowY?: overflowTypes;
  overflowX?: overflowTypes;
}

const FlexContainer = styled.div<ContainerProps>`
  display: flex;
  ${props => css`
    ${props.gutters &&
    css`
      padding: 2rem 13vw;
      @media screen and (max-width: 1400px) {
        padding: 2rem 8vw;
      }
      @media screen and (max-width: 1000px) {
        padding: 2rem 5vw;
      }
      @media screen and (max-width: 858px) {
        padding: 2rem 3vw;
      }
    `};
    ${props.disableScrollbar &&
    css`
      scrollbar-width: 0;
      ::-webkit-scrollbar {
        width: 0;
        height: 0;
      }
    `}
    align-items: ${props.alignItems ? props.alignItems : 'center'};
    flex-direction: ${props.flexDirection ? props.flexDirection : 'column'};
    flex-wrap: ${props.flexWrap ? props.flexWrap : 'initial'};
    justify-content: ${props.justifyContent ? props.justifyContent : 'center'};
    position: ${props.position ? props.position : 'relative'};
    text-align: ${props.textAlign ? props.alignItems : 'left'};
    ${props.alignSelf && `align-self: ${props.alignSelf}`};
    ${props.background && `background: ${props.background}`};
    ${props.border && `border: ${props.border}`};
    ${props.borderBottom && `border-bottom: ${props.borderBottom}`};
    ${props.borderBottomLeftRadius &&
    `border-bottom-left-radius: ${props.borderBottomLeftRadius}`};
    ${props.borderBottomRightRadius &&
    `border-bottom-right-radius: ${props.borderBottomRightRadius}`};
    ${props.borderLeft && `border-left: ${props.borderLeft}`};
    ${props.borderRadius && `border-radius: ${props.borderRadius}`};
    ${props.borderRight && `border-right: ${props.borderRight}`};
    ${props.borderTop && `border-top: ${props.borderTop}`};
    ${props.borderTopLeftRadius &&
    `border-top-left-radius: ${props.borderTopLeftRadius}`};
    ${props.borderTopRightRadius &&
    `border-top-right-radius: ${props.borderTopRightRadius}`};
    ${props.bottom && `bottom: ${props.bottom}`};
    ${props.boxShadow && `box-shadow: ${props.boxShadow}`};
    ${props.color && `color: ${props.color}`};
    ${props.cursor && `cursor: ${props.cursor}`};
    ${props.flex && `flex: ${props.flex}`};
    ${props.flexBasis && `flex-basis: ${props.flexBasis}`};
    ${props.fullWidth && 'width: 100%'};
    ${props.height && `height: ${props.height}`};
    ${props.justifySelf && `justify-self: ${props.justifySelf}`};
    ${props.left && `left: ${props.left}`};
    ${props.margin && `margin: ${props.margin}`};
    ${props.marginBottom && `margin-bottom: ${props.marginBottom}`};
    ${props.marginLeft && `margin-left: ${props.marginLeft}`};
    ${props.marginRight && `margin-right: ${props.marginRight}`};
    ${props.marginTop && `margin-top: ${props.marginTop}`};
    ${props.maxHeight && `max-height: ${props.maxHeight}`};
    ${props.maxWidth && `max-width: ${props.maxWidth}`};
    ${props.minHeight && `min-height: ${props.minHeight}`};
    ${props.minWidth && `min-width: ${props.minWidth}`};
    ${props.overflow && `overflow: ${props.overflow}`};
    ${props.overflowY && `overflow-y: ${props.overflowY}`};
    ${props.overflowX && `overflow-x: ${props.overflowX}`};
    ${props.padding && `padding: ${props.padding}`};
    ${props.paddingBottom && `padding-bottom: ${props.paddingBottom}`};
    ${props.paddingLeft && `padding-left: ${props.paddingLeft}`};
    ${props.paddingRight && `padding-right: ${props.paddingRight}`};
    ${props.paddingTop && `padding-top: ${props.paddingTop}`};
    ${props.right && `right: ${props.right}`};
    ${props.top && `top: ${props.top}`};
    ${props.transform && `transform: ${props.transform}`};
    ${props.width && `width: ${props.width}`};
    ${props.zIndex && `z-index: ${props.zIndex}`};
  `};
`;

export { FlexContainer };
