import styled, { css } from 'styled-components';
import StyleTypes, {
  textTransformTypes,
  sizeTypes,
  whiteSpaceType,
  wordBreakType
} from './Style.Types';

interface TextProps extends StyleTypes {
  before?: boolean;
  fontWeight?: number;
  lineHeight?: number;
  size?: sizeTypes;
  textDecoration?: string;
  textTransform?: textTransformTypes;
  whiteSpace?: whiteSpaceType;
  wordBreak?: wordBreakType;
}

const Title = styled.h1<TextProps>`
  font-family: 'Poppins', sans-serif;
  ${props =>
    css`
      font-size: ${props.size === 'large'
        ? 'clamp(3rem, 7vw, 4rem)'
        : props.size === 'medium'
        ? 'clamp(2.5rem, 4vw, 2.8rem)'
        : props.size === 'small'
        ? 'clamp(1.5rem, 2.5vw, 2rem)'
        : props.size === 'extra-small'
        ? 'clamp(1rem, 1.05rem, 1.1rem)'
        : props.size
        ? props.size
        : 'clamp(1.1rem, 1.5vw, 1.4rem)'};
      line-height: ${props.lineHeight ? props.lineHeight : 1.3};
      text-transform: ${props.textTransform ? props.textTransform : 'none'};
      text-decoration: ${props.textDecoration ? props.textDecoration : 'none'};
      position: ${props.position ? props.position : 'relative'};
      font-weight: ${props.fontWeight ? props.fontWeight : 700};
      text-align: ${props.textAlign ? props.textAlign : 'left'};
      line-height: ${props.lineHeight ? props.lineHeight : 1.3};
      margin: ${props.margin ? props.margin : 0};
      padding: ${props.padding ? props.padding : 0};
      ${props.background && `background: ${props.background}`};
      ${props.border && `border: ${props.border}`};
      ${props.borderBottom && `border-bottom: ${props.borderBottom}`};
      ${props.borderLeft && `border-left: ${props.borderLeft}`};
      ${props.borderRight && `border-right: ${props.borderRight}`};
      ${props.borderTop && `border-top: ${props.borderTop}`};
      ${props.bottom && `bottom: ${props.bottom}`};
      ${props.color && `color: ${props.color}`};
      ${props.cursor && `cursor: ${props.cursor}`};
      ${props.flexBasis && `flex-basis: ${props.flexBasis}`};
      ${props.fullWidth && 'width: 100%'};
      ${props.height && `height: ${props.height}`};
      ${props.left && `left: ${props.left}`};
      ${props.marginBottom && `margin-bottom: ${props.marginBottom}`};
      ${props.marginLeft && `margin-left: ${props.marginLeft}`};
      ${props.marginRight && `margin-right: ${props.marginRight}`};
      ${props.marginTop && `margin-top: ${props.marginTop}`};
      ${props.maxWidth && `max-width: ${props.maxWidth}`};
      ${props.minWidth && `min-width: ${props.minWidth}`};
      ${props.paddingBottom && `padding-bottom: ${props.paddingBottom}`};
      ${props.paddingLeft && `padding-left: ${props.paddingLeft}`};
      ${props.paddingRight && `padding-right: ${props.paddingRight}`};
      ${props.paddingTop && `padding-top: ${props.paddingTop}`};
      ${props.right && `right: ${props.right}`};
      ${props.top && `top: ${props.top}`};
      ${props.whiteSpace && `white-space: ${props.whiteSpace}`};
      ${props.width && `width: ${props.width}`};
      ${props.wordBreak && `word-break: ${props.wordBreak}`};
      z-index: ${props.zIndex ? props.zIndex : 0};
      ${props.before &&
      css`
        ::before {
          content: '';
          position: absolute;
          top: -0.3rem;
          left: -1rem;
          width: 3rem;
          height: 0.7rem;
          border-radius: ${props.theme.borderRadius};
          background: ${props.theme.primary.base};
        }
      `};
    `}
`;

const Text = styled.p<TextProps>`
  font-family: 'Poppins', sans-serif;
  ${props =>
    css`
      font-size: ${props.size === 'large'
        ? 'clamp(1.6rem, 1.8rem, 2rem)'
        : props.size === 'medium'
        ? 'clamp(1.3rem, 1.4rem, 1.5rem)'
        : props.size === 'small'
        ? 'clamp(0.8rem, 0.85rem, 0.9rem)'
        : props.size === 'extra-small'
        ? 'clamp(0.65rem, 0.7rem, 0.8rem)'
        : props.size
        ? props.size
        : 'clamp(1rem, 1rem, 1.1rem)'};
      text-transform: ${props.textTransform ? props.textTransform : 'none'};
      text-decoration: ${props.textDecoration ? props.textDecoration : 'none'};
      position: ${props.position ? props.position : 'relative'};
      font-weight: ${props.fontWeight ? props.fontWeight : 400};
      text-align: ${props.textAlign ? props.textAlign : 'left'};
      line-height: ${props.lineHeight ? props.lineHeight : 1.3};
      margin: ${props.margin ? props.margin : 0};
      ${props.background && `background: ${props.background}`};
      ${props.border && `border: ${props.border}`};
      ${props.borderBottom && `border-bottom: ${props.borderBottom}`};
      ${props.borderLeft && `border-left: ${props.borderLeft}`};
      ${props.borderRight && `border-right: ${props.borderRight}`};
      ${props.borderTop && `border-top: ${props.borderTop}`};
      ${props.bottom && `bottom: ${props.bottom}`};
      ${props.color && `color: ${props.color}`};
      ${props.cursor && `cursor: ${props.cursor}`};
      ${props.flexBasis && `flex-basis: ${props.flexBasis}`};
      ${props.fullWidth && 'width: 100%'};
      ${props.height && `height: ${props.height}`};
      ${props.left && `left: ${props.left}`};
      ${props.marginBottom && `margin-bottom: ${props.marginBottom}`};
      ${props.marginLeft && `margin-left: ${props.marginLeft}`};
      ${props.marginRight && `margin-right: ${props.marginRight}`};
      ${props.marginTop && `margin-top: ${props.marginTop}`};
      ${props.maxWidth && `max-width: ${props.maxWidth}`};
      ${props.minWidth && `min-width: ${props.minWidth}`};
      ${props.paddingBottom && `padding-bottom: ${props.paddingBottom}`};
      ${props.paddingLeft && `padding-left: ${props.paddingLeft}`};
      ${props.paddingRight && `padding-right: ${props.paddingRight}`};
      ${props.paddingTop && `padding-top: ${props.paddingTop}`};
      ${props.right && `right: ${props.right}`};
      ${props.top && `top: ${props.top}`};
      ${props.whiteSpace && `white-space: ${props.whiteSpace}`};
      ${props.width && `width: ${props.width}`};
      ${props.wordBreak && `word-break: ${props.wordBreak}`};
      z-index: ${props.zIndex ? props.zIndex : 0};
    `}
`;

export { Title, Text };
