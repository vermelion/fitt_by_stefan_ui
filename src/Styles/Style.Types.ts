type globalTypes = 'inherit' | 'initial' | 'unset';
type alignItemsTypes =
  | 'stretch'
  | 'flex-start'
  | 'flex-end'
  | 'center'
  | globalTypes;
type cursorTypes =
  | 'alias'
  | 'all-scroll'
  | 'auto'
  | 'cell'
  | 'col-resize'
  | 'context-menu'
  | 'copy'
  | 'crosshair'
  | 'default'
  | 'e-resize'
  | 'ew-resize'
  | 'grab'
  | 'grabbing'
  | 'help'
  | 'move'
  | 'n-resize'
  | 'ne-resize'
  | 'nesw-resize'
  | 'no-drop'
  | 'none'
  | 'not-allowed'
  | 'ns-resize'
  | 'nw-resize'
  | 'nwse-resize'
  | 'pointer'
  | 'progress'
  | 'row-resize'
  | 's-resize'
  | 'se-resize'
  | 'sw-resize'
  | 'text'
  | 'vertical-text'
  | 'w-resize'
  | 'wait'
  | 'zoom-in'
  | 'zoom-out'
  | '';
type flexDirectionTypes =
  | 'row'
  | 'row-reverse'
  | 'column'
  | 'column-reverse'
  | globalTypes;
type flexWrapTypes = 'wrap' | 'nowrap' | 'wrap-reverse' | globalTypes;
type justifyContentTypes =
  | 'baseline'
  | 'end'
  | 'left'
  | 'right'
  | 'first baseline'
  | 'last baseline'
  | 'space-between'
  | 'space-evenly'
  | 'space-around'
  | 'stretch'
  | 'start'
  | 'safe'
  | 'flex-start'
  | 'flex-end'
  | 'center'
  | 'unsafe'
  | globalTypes;
type overflowTypes = 'hidden' | 'auto' | 'visible' | 'scroll' | globalTypes;
type positionTypes =
  | 'relative'
  | 'absolute'
  | 'fixed'
  | 'static'
  | 'sticky'
  | globalTypes;
type sizeTypes = 'extra-small' | 'small' | 'medium' | 'large' | string | globalTypes;
type textAlignTypes =
  | 'left'
  | 'right'
  | 'start'
  | 'end'
  | 'center'
  | 'justify'
  | globalTypes;
type textTransformTypes =
  | 'uppercase'
  | 'lowercase'
  | 'capitalize'
  | 'none'
  | globalTypes;
type whiteSpaceType =
  | 'nowrap'
  | 'normal'
  | 'pre'
  | 'pre-line'
  | 'pre-wrap'
  | globalTypes;
type wordBreakType = 'break-all' | 'keep-all' | 'normal' | globalTypes;

interface StyleTypes {
  background?: string | undefined;
  border?: string | number;
  borderBottom?: string | number;
  borderLeft?: string | number;
  borderRight?: string | number;
  borderTop?: string | number;
  bottom?: number | string;
  color?: string;
  cursor?: cursorTypes;
  flex?: number;
  flexBasis?: string;
  fullWidth?: boolean;
  height?: string;
  left?: number | string;
  margin?: string | number;
  marginBottom?: string | number;
  marginLeft?: string | number;
  marginRight?: string | number;
  marginTop?: string | number;
  maxHeight?: string;
  maxWidth?: string;
  minHeight?: string;
  minWidth?: string;
  padding?: string | number;
  paddingBottom?: string | number;
  paddingLeft?: string | number;
  paddingRight?: string | number;
  paddingTop?: string | number;
  position?: positionTypes;
  right?: number | string;
  textAlign?: textAlignTypes;
  top?: number | string;
  transform?: string;
  width?: string;
  zIndex?: number;
}

export type {
  StyleTypes as default,
  alignItemsTypes,
  cursorTypes,
  flexDirectionTypes,
  flexWrapTypes,
  justifyContentTypes,
  overflowTypes,
  positionTypes,
  sizeTypes,
  textAlignTypes,
  textTransformTypes,
  whiteSpaceType,
  wordBreakType
};
