export { FlexContainer } from './Container.Style';
export { Title, Text } from './Text.Style';
export type {
  flexDirectionTypes,
  flexWrapTypes,
  alignItemsTypes,
  justifyContentTypes,
  textAlignTypes,
  sizeTypes
} from './Style.Types';
