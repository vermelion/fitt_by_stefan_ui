interface ResultProps {
  result: 'success' | 'error';
  frontEndMessage: string;
  programmerMessage: string;
  data: any;
}

export type { ResultProps };
