const useGuidsLocalStorage = () => {
  const body = {
    member_guid: localStorage.getItem('MemberGuid'),
    session_guid: localStorage.getItem('SessionGuid')
  };
  const query = `?member_guid=${localStorage.getItem(
    'MemberGuid'
  )}&session_guid=${localStorage.getItem('SessionGuid')}`;
  return { body, query };
};

export { useGuidsLocalStorage as default };
