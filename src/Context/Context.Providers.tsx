import React from 'react';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';

import { useThemeConstant } from '../Constants';

const Providers: React.FC<{ children?: React.ReactNode }> = ({ children }) => {
  const Theme = useThemeConstant();

  const muiTheme = createTheme({
    palette: {
      primary: { main: Theme.primary.base },
      secondary: { main: Theme.secondary.base },
      type: 'dark'
    }
  });
  
  return (
    <StyledThemeProvider theme={Theme}>
      <ThemeProvider theme={muiTheme}>{children}</ThemeProvider>
    </StyledThemeProvider>
  );
};
export default React.memo(Providers);
