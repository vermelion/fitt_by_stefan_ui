import React from 'react';
import { PlanPage } from '../../Components';
import PromoPoster from '../../Assets/images/Poster.png';
import PromoVideo from '../../Assets/videos/Promo.mp4';
import { useServicesConstant, useTestimonialsConstant } from '../../Constants';

const Plan12weekPage: React.FC = () => {
  const testimonials = useTestimonialsConstant();
  const services = useServicesConstant();
  return (
    <React.Fragment>
      <PlanPage
        title='12 WEEK TRANSFORMATION'
        slogan='"Summer bodies are made in Winter"'
        videoCaption='Stefan van der merwe - Bsc. Hons Biokinetics graduate,
            Sport Science graduate, Certified Personal trainer and Founder of
            @fittbystefan'
        // videoCaption={() => (
        //   <>
        //     <strong>Stefan van der merwe</strong> - Bsc. Hons Biokinetics graduate,
        //     Sport Science graduate, Certified Personal trainer and Founder of
        //     @fittbystefan
        //   </>
        // )}
        PromoPoster={PromoPoster}
        PromoVideo={PromoVideo}
        testimonials={[
          testimonials.Francois,
          testimonials.Minki,
          testimonials.MarkLukas,
          testimonials.Mishka
        ]}
        services={services.Plan12Week}
      />
    </React.Fragment>
  );
};

export default React.memo(Plan12weekPage);
