import React from 'react';
import { FlexContainer, Text, Title } from '../../Styles';

const HomePage: React.FC = () => {
  return (
    <FlexContainer gutters>
      <Title>HOME</Title>
    </FlexContainer>
  );
};
export default React.memo(HomePage);
