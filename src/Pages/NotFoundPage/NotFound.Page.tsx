import React from 'react';
import { Title } from '../../Styles';

const NotFoundPage: React.FC = () => {
  return (
    <div>
      <Title size='large'>Page Not Found</Title>
    </div>
  );
};
export default React.memo(NotFoundPage);
