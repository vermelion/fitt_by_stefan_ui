export { default as HomePage } from './HomePage/Home.Page';
export { default as NotFoundPage } from './NotFoundPage/NotFound.Page';
export { default as Plan12weekPage } from './Plan12weekPage/Plan.12week.Page';
