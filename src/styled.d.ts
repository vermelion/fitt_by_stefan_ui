import 'styled-components';

interface ThemeBaseProps {
  base: string;
  accent: string;
  light: string;
  dark: string;
  darker: string;
}

interface CardBackgroundProps {
  primary: string;
  secondary: string;
  secondary2: string;
}

interface ThemeProps {
  background: string;
  cardBackground: CardBackgroundProps;
  primary: ThemeBaseProps;
  secondary: ThemeBaseProps;
  secondary2: ThemeBaseProps;
  due: string;
  late: string;
  completed: string;
  completedLate: string;
  text: string;
}
// and extend them!
declare module 'styled-components' {
  export interface DefaultTheme {
    background: string;
    borderRadius: string;
    borderRadiusSmall: string;
    cardBackground: CardBackgroundProps;
    completed: string;
    completedLate: string;
    due: string;
    late: string;
    primary: ThemeBaseProps;
    secondary: ThemeBaseProps;
    secondary2: ThemeBaseProps;
    text: string;
  }
}
