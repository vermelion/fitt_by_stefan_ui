import React from 'react';
import { ServiceProps, TestimonialProps } from '../../Constants';
import { FlexContainer, Text, Title } from '../../Styles';
import { ServicesGrid, TestimonialsGrid, Video } from './Page.Plan.Style';
import TestimonialPlan from './Testimonial.Plan.Component';
import ServicePlan from './Service.Plan.Component';

interface PlanPageComponentProps {
  title: string;
  slogan: string;
  videoCaption: string | any;
  PromoPoster: string;
  PromoVideo: string;
  testimonials: Array<TestimonialProps>;
  services: Array<ServiceProps>;
}

const PlanPageComponent: React.FC<PlanPageComponentProps> = ({
  title,
  slogan,
  videoCaption,
  PromoPoster,
  PromoVideo,
  testimonials,
  services
}) => {
  return (
    <FlexContainer margin='2rem auto' width='95vw' maxWidth='70rem'>
      <Title size='large'>{title}</Title>
      <Title size='small'>{slogan}</Title>
      <FlexContainer margin='2rem 0'>
        <Video poster={PromoPoster} controls>
          <source src={PromoVideo} type='video/mp4' />
        </Video>
        <Text maxWidth='50rem' textAlign='center' size='small' marginTop='1em'>
          {videoCaption}
        </Text>
        {/* <Text>{videoCaption()}</Text> */}
      </FlexContainer>
      <TestimonialsGrid>
        {testimonials.map(testimonial => (
          <TestimonialPlan {...testimonial} />
        ))}
      </TestimonialsGrid>
      <ServicesGrid>
        {services.map(service => (
          <ServicePlan {...service} />
        ))}
      </ServicesGrid>
    </FlexContainer>
  );
};

export default React.memo(PlanPageComponent);
