import styled from 'styled-components';

const Video = styled.video`
  cursor: pointer;
  border-radius: 1rem;
  width: 100%;
  /* height: 15rem;
  @media screen and (min-width: 530px) {
    height: 20rem;
  }
  @media screen and (min-width: 700px) {
    height: 25rem;
  }
  @media screen and (min-width: 858px) {
    height: 30rem;
  }
  @media screen and (min-width: 1000px) {
    height: 35rem;
  }
  @media screen and (min-width: 1200px) {
    height: 40rem;
  } */
`;

const TestimonialsGrid = styled.div`
  margin: 2rem 0;
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 2.5rem;
  align-items: stretch;
  justify-content: stretch;
  @media screen and (max-width: 750px) {
    grid-template-columns: 1fr;
    grid-gap: 0;
  }
`;
const ServicesGrid = styled.div`
  margin: 2rem 0;
  width: 100%;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-gap: 2.5rem;
  align-items: stretch;
  justify-content: stretch;
  @media screen and (max-width: 750px) {
    grid-template-columns: 1fr;
    grid-gap: 0;
  }
`;

export { Video, TestimonialsGrid, ServicesGrid };
