import React from 'react';
import { ServiceProps } from '../../Constants';
import { FlexContainer, Text, Title } from '../../Styles';
import CheckCircle from '../../Assets/images/CheckCircle.png';
import { Service, ServiceIcon } from './Service.Plan.Style';
const ServicePlanComponent: React.FC<ServiceProps> = service => {
  return (
    <Service>
      <ServiceIcon>
        <img src={CheckCircle} alt={CheckCircle} />
      </ServiceIcon>
      <Title margin='0.5rem 0'>{service.title}</Title>
      <Text>{service.service}</Text>
    </Service>
  );
};

export default React.memo(ServicePlanComponent);
