import styled from 'styled-components';
import { FlexContainer } from '../../Styles';

const Testimonial = styled(FlexContainer)`
  width: 100%;
  position: relative;
  padding: 1.5rem;
  border-radius: 5px;
  @media screen and (max-width: 750px) {
    border-radius: 0;
  }
`;


const Profile = styled.div`
  cursor: pointer;
  display: table;
  min-width: 3rem;
  min-height: 3rem;
  max-width: 3rem;
  max-height: 3rem;
  overflow: hidden;
  border-radius: 50%;
  img {
    box-shadow: 3px 3px 6px rgba(10, 10, 10, 0.2);
    min-width: 3rem;
    max-width: 3rem;
    min-height: 3rem;
    max-height: 3rem;
    object-fit: cover;
    object-position: center;
  }
  @media screen and (max-width: 1100px) {
    margin-bottom: 1rem;
  }
`;

const ViewProfile = styled.div`
  width: 100%;
  overflow: hidden;
  img {
    width: 100%;
    object-fit: cover;
    object-position: center;
  }
`;

export { Testimonial, Profile, ViewProfile };
