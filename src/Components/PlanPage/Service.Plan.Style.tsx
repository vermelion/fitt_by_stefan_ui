import styled from 'styled-components';
import { FlexContainer } from '../../Styles';

const Service = styled(FlexContainer)`
  padding: 1rem 1.5rem;
  box-shadow: 0 0 8px 0 rgba(0, 0, 0, 0.2);
  border-radius: 15px;
  transition: transform 0.4s ease, box-shadow 0.4s ease;
  :hover {
    box-shadow: 2px 0 15px 1px rgba(0, 0, 0, 0.2);
    /* background: #cda4d4; */
    transform: scale(1.01);
  }
`;

const ServiceIcon = styled.div`
  width: 4rem;
  height: 4rem;
  
  overflow: hidden;
  img {
    object-fit: cover;
    object-position: center;
    min-width: 4rem;
    max-width: 4rem;
    min-height: 4rem;
    max-height: 4rem;
  }
`;
export { Service, ServiceIcon };
