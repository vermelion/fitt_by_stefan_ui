import React from 'react';
import { FlexContainer, Text } from '../../Styles';
import { Testimonial, Profile, ViewProfile } from './Testimonial.Plan.Style';
import { Dialog, Zoom } from '@material-ui/core';
import { TransitionProps } from '@material-ui/core/transitions/transition';
import { TestimonialProps } from '../../Constants';

const PlanTestimonialComponent: React.FC<TestimonialProps> = testimonial => {
  const [showImage, setShowImage] = React.useState<boolean>(false);
  const showImageHandler = React.useCallback(
    () => setShowImage(prevShowImage => !prevShowImage),
    []
  );
  return (
    <>
      <Testimonial background='#e8e9f2d5'>
        <FlexContainer
          background='#fefefe'
          padding='1rem'
          borderRadius='15px'
          alignItems='flex-start'
          height='100%'
        >
          <Text>{testimonial.testimonial}</Text>
          <FlexContainer marginTop='1.5rem' flexDirection='row'>
            <Profile onClick={showImageHandler}>
              <img src={testimonial.profile} alt={testimonial.profile} />
            </Profile>
            <FlexContainer marginLeft='1rem' alignItems='flex-start'>
              <Text size='small'>{testimonial.name}</Text>
              <Text size='extra-small'>{testimonial.occupation}</Text>
            </FlexContainer>
          </FlexContainer>
        </FlexContainer>
      </Testimonial>
      <Dialog
        open={showImage}
        TransitionComponent={Transition}
        keepMounted
        onClose={showImageHandler}
        aria-labelledby={`View ${testimonial} profile image`}
        aria-describedby={`View ${testimonial} profile image`}
        maxWidth='md'
        fullWidth
      >
        <ViewProfile>
          <img src={testimonial.profile} alt={testimonial.profile} />
        </ViewProfile>
      </Dialog>
    </>
  );
};

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>
) {
  return <Zoom ref={ref} {...props} />;
});

export default React.memo(PlanTestimonialComponent);
