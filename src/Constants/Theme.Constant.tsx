import { useMemo } from 'react';
import { DefaultTheme } from 'styled-components';

const ThemeConstant: DefaultTheme = {
  borderRadius: '1rem',
  borderRadiusSmall: '5px',
  background: '#ccc',
  cardBackground: {
    primary: '#333',
    secondary: '#555',
    secondary2: '#777'
  },
  completed: '#00DD3A',
  completedLate: '#008C25',
  due: '#00A5CC',
  late: '#e22828',
  primary: {
    base: '#002ED4',
    accent: '#0B40FF',
    light: '#2A57FC',
    dark: '#0023A2',
    darker: '#001C7F'
  },
  secondary: {
    base: '#00A5CC',
    accent: '#03CFFF',
    light: '#14CFFB',
    dark: '#007793',
    darker: '#005E74'
  },
  secondary2: {
    base: '#00DD3A',
    accent: '#00FF43',
    light: '#0EFD4C',
    dark: '#00B22F',
    darker: '#008C25'
  },
  text: '#222'
};

const useThemeConstant: () => DefaultTheme = () => {
  const memoizedTheme = useMemo(() => {
    return ThemeConstant;
  }, []);
  return memoizedTheme;
};

export { ThemeConstant as default, useThemeConstant };
