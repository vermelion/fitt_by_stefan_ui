import { useMemo } from 'react';
import Francious from '../Assets/images/Testimonials/Francious.jpg';
import Minki from '../Assets/images/Testimonials/Minki.jpg';
import Mishka from '../Assets/images/Testimonials/Mishka.jpg';
import MarkLukas from '../Assets/images/Testimonials/MarkLukas.jpg';

interface TestimonialProps {
  testimonial: string;
  name: string;
  occupation: string;
  profile: string;
}

interface TestimonialsConstantProps {
  Francois: TestimonialProps;
  Minki: TestimonialProps;
  Mishka: TestimonialProps;
  MarkLukas: TestimonialProps;
}

const TestimonialsConstant: TestimonialsConstantProps = {
  Francois: {
    testimonial:
      'Ek dink die groot problem met oefen en eetplanne is dat dit gewoonlik ‘n one size fits all plan is, maar Stefan skryf vir jou ‘n oefen en eetplan wat spesifiek vir jou liggaam en kondiesies is om vir jou die beste resultate te gee.',
    name: 'Francois van Coke',
    occupation: 'Singer',
    profile: Francious
  },
  Minki: {
    testimonial:
      'Ek is stom geslaan deur die kwaliteit van Stefan se oefen en eetplan, dis so eenvoudig om te vol en die gewigsverlies resultate praat vir hulself!',
    name: 'Minki Van der Westhuizen',
    occupation: 'Influencer',
    profile: Minki
  },
  Mishka: {
    testimonial:
      'I believed that I was just meant to be overweight, it’s in my genetics – I told myself. Stefan showed me that I didn’t need to starve myself to loose weight, he is such a great coach! I have so much confidence now! Can’t even begin to say how lifechanging his program was for me! Thank you Stefan!',
    name: 'Mishka',
    occupation: 'Customer',
    profile: Mishka
  },
  MarkLukas: {
    testimonial:
      'I honestly could not believe how fast the fat melted off of my body! I always thought that I would have to spend hours in the gym and eat bland boring food to loose weight. Now I know this is not true! The food was great, easy to make and very affordable! Stefan’s program is fantastic and I can definitely recommend it! JUST DO IT!',
    name: 'Mark Lukas',
    occupation: 'Customer',
    profile: MarkLukas
  }
};

const useTestimonialsConstant: () => TestimonialsConstantProps = () => {
  const memoizedTestimonials = useMemo(() => {
    return TestimonialsConstant;
  }, []);
  return memoizedTestimonials;
};

export { TestimonialsConstant as default, useTestimonialsConstant };
export type { TestimonialProps };
