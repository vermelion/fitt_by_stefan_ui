import { useMemo } from 'react';
import { NavigationTypes } from './Navigation.Type';
import { Plan12weekPage } from '../../Pages';

interface NavigationProps {
  path: NavigationTypes;
  component: React.NamedExoticComponent<{}>;
}

interface NavigationRoutesConstantProps {
  public: Array<NavigationProps>;
}

const NavigationRoutesConstant: NavigationRoutesConstantProps = {
  public: [
    {
      path: '/',
      component: Plan12weekPage
    },
    {
      path: '/12week-plan',
      component: Plan12weekPage
    }
  ]
};

const useNavigationRoutesConstant = () => {
  const memoizedNavigations = useMemo(() => {
    return NavigationRoutesConstant;
  }, []);
  return memoizedNavigations;
};

export { NavigationRoutesConstant as default, useNavigationRoutesConstant };
