import { useMemo } from 'react';

interface ServiceProps {
  service: string;
  title: string;
}

interface ServicesConstantProps {
  Plan12Week: Array<ServiceProps>;
}

const ServicesConstant: ServicesConstantProps = {
  Plan12Week: [
    {
      title: 'Extremely effective',
      service:
        'An extremely effective transformation workout plan (for in the gym or at home training)'
    },
    {
      title: 'Melt away fat',
      service:
        'A mealplan that will help you melt away fat (made for your body type and goals)'
    },
    {
      title: 'Whatsapp support group',
      service:
        'A whatsapp support group! A group of likeminded people, ready to take on this 8 week journey with you!'
    },
    {
      title: 'Online coach',
      service:
        'Me as your online coach- keeping you accountable, making sure that you reach your transformation goals!'
    },
    {
      title: 'Most complete',
      service:
        "The most complete and most effective program I've ever written. You will never need another program again!"
    }
  ]
};

const useServicesConstant: () => ServicesConstantProps = () => {
  const memoizedServices = useMemo(() => {
    return ServicesConstant;
  }, []);
  return memoizedServices;
};

export { ServicesConstant as default, useServicesConstant };
export type { ServiceProps };
